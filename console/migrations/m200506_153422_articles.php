<?php

use yii\db\Migration;

/**
 * Class m200506_153422_articles
 */
class m200506_153422_articles extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('articles' , [
           'id' => $this->primaryKey(),
           'title' => $this->string(),
           'text' => $this->text(),
           'author_id' => $this->integer(),
           'alias' => $this->string(200), 
           'data' => $this->date("Y-m-d"),
           'likes' => $this->integer(),
           'hits' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200506_153422_articles cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200506_153422_articles cannot be reverted.\n";

        return false;
    }
    */
}
