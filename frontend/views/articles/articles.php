<?php
use yii\helpers\Url;
?>

<div class="site-index">
    <div class="jumbotron">
        <h1 >This a title for page</h1>
    </div>
    <div class="row">
        <?php /** @var \frontend\models\Articles $articles */
        foreach($articles as $article): ?>
        <div class="col-lg-6">
            <h2><a href="<?= Url::to(['articles/article', 'id' => $article->id]) ?>"><?= $article->title ?></a></h2>
            <p><?=$article->getShortText($article->text) ?></p>
            <p><a class="btn btn-success" href="<?=Url::to(['articles/article', 'id' => $article->id]) ?>">See more >></a></p>
            <hr>
        </div>
        <?php endforeach; ?>
    </div>

</div>
