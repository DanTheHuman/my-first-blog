<?php
use yii\helpers\Url;

?>
<a href="<?=Url::to('articles') ?>" class="btn btn-success"><< Back to articles <list></list></a>
<br>
<div>
    <h1><?php /** @var \frontend\models\Articles $article */
       echo $article->getFullTitle($article->title) ?></h1>
    <p><?php echo $article->text ?></p>
    <span><?php echo $article->author_id ?> <?php echo $article->data ?></span>
    <div style="float: right;display: inline-flex">
        <i class="material-icons" style="color:red;margin-right: 3px;">favorite_border</i>
        <p style="margin-right: 1vw"><?php echo $article->getDescription($article->likes, 'like') ?></p>
        <i class="material-icons" style="color: grey;margin-right: 3px;">perm_identity</i>
        <p><?php echo $article->getDescription($article->hits, 'hits') ?></p>
    </div>
</div>
