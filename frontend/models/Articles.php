<?php


namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;

/**
 * @property string title
 */
class Articles extends ActiveRecord
{
    public function getFullTitle($title) {
        return 'Article: ' . $title;
    }

    public function getShortText($text){
        $text = mb_substr($text, 0, 300);
        $firePos = strripos($text,' ');
        $text = mb_substr($text, 0, $firePos);
        return $text . '...';
    }

    public function getDescription($hits, $value) {
        $description = array(
            'like' => array('лайк', 'лайка', 'лайков'),
            'hits' => array('просмотр', 'просмотра', 'просмотров')
        );

        if( mb_substr($hits, -1) == 1 && mb_substr($hits, -2) != 11) {
            return $hits . ' ' . $description[$value][0];
        }else if (mb_substr($hits, -1) > 1 && mb_substr($hits, -1) < 5 && (mb_substr($hits, -2) < 5 || mb_substr($hits, -2) > 15)){
            return $hits . ' ' . $description[$value][1];
        } else {
          return $hits . ' ' . $description[$value][2];
        }
    }
}